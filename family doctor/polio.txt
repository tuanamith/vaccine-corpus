What is polio?

Poliomyelitis (polio, for short) is a serious illness that can cause paralysis (when you can’t move your arms and legs) or even death. Polio is caused by a virus. The virus can be spread by drinking water with the polio virus in it. It can also be passed by close contact, such as kissing, with an infected person. Before the first polio vaccine was developed in the 1950s, thousands of children got polio every year. Fortunately, the use of the polio vaccine has made the disease very rare in most parts of the world.

How can polio be prevented?

You can keep your children from getting polio by making sure they get the polio vaccine.
What is the polio vaccine?

A vaccine is used to protect you from getting a disease. The polio vaccine, also called IPV, is given by injection (a "shot"). (It used to be given by drops in the mouth.)

When should my child be vaccinated?

Most children get 4 doses of polio vaccine on this schedule:

    First dose when they are 2 months old.
    Second dose when they are 4 months old.
    Third dose when they are 6 to 18 months old.
    Last dose when they are 4 to 6 years old.

Are there reasons not to get polio shots?

Your child should not get the polio shots if he or she is allergic to these medicines: neomycin, streptomycin or polymyxin B.

What are the risks of the vaccine?

Most people have no problems. Some people will have some pain or redness where the shot was given. Vaccines carry a small risk of serious harm, such as a severe allergic reaction.

The polio vaccine doesn’t cause polio.

What if my child has a reaction to the vaccine?

If your child has any reaction after getting the polio vaccine, call your doctor as soon as possible. In addition, if your child gets hives (swelling, itching and a burning sensation of the skin), has problems breathing, or goes into shock (becomes weak, faint, cold, clammy and sweaty), call 911 or take your child to a hospital emergency room. Be sure to tell the doctors caring for your child the day and time your child received the vaccine. Ask your doctor to file a Vaccine Adverse Event Report form with the CDC, or you can do it yourself by calling 800-822-7967.