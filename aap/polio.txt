The Disease
Polio was one of the most dreaded childhood diseases of the 20th Century. An epidemic in the US in 1916 killed 6,000 people and left 27,000 more paralyzed. In the 1950's, parents refused to let their children go to movies or go swimming for fear of catching the disease.
The Immunization
There are two types of polio vaccine: Inactivated (killed) polio vaccine (IPV), which is a shot; and live oral polio vaccine (OPV), which is a liquid that is swallowed. In the US, only the IPV is used.
